###################################################
#install.packages("bib2df")

#library(bib2df) # This package read BibTex format

###########################
# read  csv file from pubmed

library(dplyr)

find_Dupes <- function(fileName1, fileName2) {
  
df1<-read.csv(fileName1, header = T)
df2<-read.csv(fileName2,  header = T)
  
# select variables and reorder:
df1<-select(df1, Authors, Journal.Book, Title, Publication.Year, DOI)
df2<-select(df2, Authors, Journal.Book, Title, Publication.Year, DOI)

# -------------------------------------------------------------------------

# Change variables names to match with df1 and df2:
names<-c("AUTHOR","JOURNAL", "TITLE", "YEAR","DOI")
colnames(df1) <- names
colnames(df2) <- names

str(df1)

##########################
# join the 2 files:

res <- dplyr::bind_rows(df1,df2)

##########################
# removing duplicated entries, using DOI variable:
res_sem_dupl<-res %>% distinct(DOI, .keep_all = TRUE)

# Find the removed (a duplicates
duplicate_dois <- res %>%
  group_by(DOI) %>%
  filter(n() > 1) %>%
  select(DOI) %>%
  distinct()

num_of_duplicates <- nrow(duplicate_dois)

# Extract rows that are duplicates
removed_items <- semi_join(res, duplicate_dois, by = "DOI")

########################
# order variables by title
dados<-arrange(res_sem_dupl,TITLE)

########################
# save the dataframe
library(openxlsx)

#write.xlsx(dados, "results/results.xlsx", sheetName="Plan1", colNames=T, rowNames=F,append=F)

return(list(num_of_duplicates, duplicate_dois))
}



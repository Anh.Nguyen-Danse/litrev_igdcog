library(dplyr)
library(bib2df)
library(tidyr)
library(ggplot2)
library(gt)
library(stringr)
library(wordcloud)

path_to_scripts <-"H:/Repositories/litrev_igdcog/my_functions"
r_files <- list.files(path = path_to_scripts, pattern ="\\.R$", full.names =TRUE)
lapply(r_files, source)


potentialArticles <- "H:/Searches/updated pot articles 8_10.csv" #insert potential articles from Zotero
excel <- "H:/Searches/Search progress(simple) 10_10.csv" #insert potential articles from excel

excel_compare <-  "H:/Searches/Search progress(Sheet1).csv" #TOTAL SEARCHES FROM EXCEL
search_compare <- "H:/Searches/csv-gamingANDl-set ATTENTION.csv" #INSERT SEARCH FROM PUBMED (ETC.)

#----------------------------------------------------------------------------
# Function to find the duplicates between two sets of articles from pubmed 
#numOfDupes <- find_Dupes(excel_compare,search_compare) 

# Function to find the duplicates between a PubMed search and the Search Progress excel file
find_dupes_all_searches(excel_compare, search_compare)

# Function to receive a table of the most common keywords from abstracts
absCount <- get_keywords_fromAbs(potentialArticles)

find_DOI_Differences(potentialArticles, excel)

# Function to receive info on titles, authors, publication years, population types, continents of studies, study durations, and assessments
# Format: find_all_info(mendeleyFile, excelSearchProgressSimpleFile)
find_all_info(potentialArticles, excel)
